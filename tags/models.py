from django.db import models


class Tag(models.Model):
    name = models.CharField(max_length=20)
    recipes = models.ForeignKey("recipes.Recipe", related_name="tags", on_delete=models.CASCADE,)

    def __str__(self):
        return self.name
